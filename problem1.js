const fs = require('fs');
const { resolve } = require('path');

function getRandomStringArray(arrayLength, wordLength = 4) {
    let outputArray = new Array(arrayLength).fill('');
    outputArray.forEach((randomString, index, array) => {
        array[index] = Math.random().toString(36).substring(2, wordLength);
    });
    return outputArray;
};

function createAndDeleteRandomJson(numberOfFiles = 0) {
    let dir = './output';
    let data = 'Hello World!\n';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);``
    }
    const fileNames = getRandomStringArray(numberOfFiles, 6);
    fileNames.forEach((file) => {
        let filePath = dir + '/' + file + '.json';
        let createFilePromise = new Promise((resolve, reject) => {
            fs.writeFile(filePath, data, 'utf-8', function(err){
                if(err){
                    reject(err);
                } else {
                    console.log('created ' + filePath);
                    resolve(filePath);
                }
            }) 
        }).then((filePath) => {
            fs.unlink(filePath, function(err) {
                if(err){
                    console.log(err);
                } else {
                    console.log('deleted ' + filePath);
                }
            })
        }).catch((err) => console.log(err));
    })
}

module.exports = { createAndDeleteRandomJson }