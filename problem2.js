const { rejects } = require('assert');
const fs = require('fs');
const { resolve } = require('path');

function writeToFileAppendName(readFrom, writeTo, appendTo, processText = (textContent) => textContent) {
    return new Promise((resolve, reject) => {
        fs.readFile(readFrom, 'utf-8', (err, fileContent) => {
            if (err) {
                reject('no such file ' + readFrom);
            } else {
                console.log('read ' + readFrom);
                resolve(fileContent);
            }
        })
    }).then((content) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(writeTo, processText(content), 'utf-8', (err) => {
                if (err) {
                    console.log(err);
                    reject(err);
                } else {
                    console.log('wrote to ' + writeTo);
                    resolve();
                }
            })
        })
    }).then(() => {
        return new Promise((resolve, reject) => {
            fs.appendFile(appendTo, writeTo + '\n', 'utf-8', (err) => {
                if (err) {
                    reject(err);
                } else {
                    console.log('appended to ' + appendTo);
                    resolve();
                }
            })
        })
    }).catch((err) => {
        console.log(err);
    })
}


function deleteFileInFileContent(readFrom) {

    new Promise((resolve, reject) => {
        fs.readFile(readFrom, 'utf-8', (err, fileNames) => {
            if (err) {
                reject('no such file ' + readFrom);
            } else {
                resolve(fileNames);
            }
        })
    }).then((fileNames) => {
        fileNames.trim().split('\n').forEach((fileName) => {
            fs.unlink(fileName, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('removed ' + fileName);
                }
            })
        });
    }).catch((err) => {
        console.log(err);
    })
}

function processLipsum(lipsumFile) {
    let lowerCaseFile = './lowercase.txt';
    let upperCaseFile = './uppercase.txt';
    let fileNamesFile = './filenames.txt';
    let sortedFile = './sortedcontent.txt';
    let lowercasePromise = writeToFileAppendName(lipsumFile, upperCaseFile, fileNamesFile, (content) => content.toUpperCase())
        .then(() => {
            writeToFileAppendName(upperCaseFile, lowerCaseFile, fileNamesFile, (content) => content.toLowerCase())
                .then(() => {
                    writeToFileAppendName(lowerCaseFile, sortedFile, fileNamesFile, (content) => content.split('.').sort().join(''))
                        .then(() => {
                            deleteFileInFileContent(fileNamesFile);
                        });
                })
        });
}
module.exports = {
    writeToFileAppendName,
    deleteFileInFileContent,
    processLipsum
}